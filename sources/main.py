########## Importer les modules necessaires ##############
from tkinter import *
from random import *
import time
from tkinter.font import Font
from random import randrange
##########################################################
##########    Fonctions ##################################
########################################################## 



def compteur_tour():
    global compteur
    compteur += 1
    Com.configure(text=compteur)
    if compteur == 3:  # Mettez ici la condition de victoire
        # Arrêtez le programme ou effectuez d'autres actions nécessaires
        Mafenetre.destroy()  # Ferme la fenêtre Tkinter
        Recomp = Tk()
        Recomp.title("Félicitations")
        Canevas = Canvas(Recomp, width=1000, height=1000, bg='white')
        Canevas.pack()
        Canevas.create_rectangle(50, 50, 950, 300, fill='lightblue', outline='black', width=2)
        message = "Bravo! Vous avez réussi!"        
        Canevas.create_text(500, 175, text=message, font=police, fill='darkblue')

        
        

def compteur_tour2():
    global compteur2
    compteur2 += 1
    Com2.configure(text=compteur2)
    if compteur2 == 3:  # Mettez ici la condition de victoire
        # Arrêtez le programme ou effectuez d'autres actions nécessaires
        Mafenetre.destroy()  # Ferme la fenêtre Tkinter
        Recomp2 = Tk()
        Recomp2.title("Félicitations")
        Canevas = Canvas(Recomp2, width=1000, height=1000, bg='white')
        Canevas.pack()
        Canevas.create_rectangle(50, 50, 950, 300, fill='lightblue', outline='black', width=2)
        message = "Bravo! Vous avez réussi!"       
        Canevas.create_text(500, 175, text=message, font=police, fill='darkblue')

        


    


def aff():
    global Time
    Time += 1
    minute=Time//60
    seconde=Time%60
    te='Temps '+str(minute)+':'+str(seconde)
    Lab.configure(text=te)  # Mettre a jour le texte 

    Mafenetre.after(1000, aff) # stopper la fonction avec un False pour que le temps freeze
        
    
def affiche(L):                         #Grille avec couleur
    i=0
    while i<50:
        j=0
        while j<45: 
            if L[j][i]==1:
                couleur='beige'
            elif L[j][i]==3:
                couleur='green'
            elif L[j][i]==4:
                couleur='white'
            elif L[j][i]==5:
                couleur='black'
            elif L[j][i]==6:
                couleur='yellow'
            else:
                couleur='gray'
            Canevas.create_rectangle(50+25*i,50+25*j,75+25*i,75+25*j,fill=couleur)
            j=j+1      
        i=i+1
      
        '''
        Déplacement
        '''
def fctDroite(ev=None):
    global compteur
    global compteur_tour
    [a, b, c, d] = Canevas.coords(image)              #Mouvement Droite 
    i = int((a - 50) // 25)
    j = int((b - 50) // 25)
    
    if L[j][i + 1] in {0, 6, 4}:
        Canevas.coords(image, a + 25, b, c + 25, d)
        if L[j][i + 1] == 6:
            command=compteur_tour()
            print("Nombre de tours rouge :", compteur)              #Compteur 
    

def fctHaut(ev=None):
    [a, b, c, d] = Canevas.coords(image)                      #Mouvement Haut
    i = int((a - 50) // 25)
    j = int((b - 50) // 25)
    
    if L[j - 1][i] in {0, 6, 4}:
        Canevas.coords(image, a, b - 25, c, d - 25)
    

def fctBas(ev=None):
    [a, b, c, d] = Canevas.coords(image)                    #Mouvement Bas
    i = int((a - 50) // 25)
    j = int((b - 50) // 25)
    
    if L[j + 1][i] in {0, 6, 4}:
        Canevas.coords(image, a, b + 25, c, d + 25)
    

def fctGauche(ev=None):
    [a, b, c, d] = Canevas.coords(image)                    #Mouvement Gauche
    i = int((a - 50) // 25)
    j = int((b - 50) // 25)
    
    if L[j][i - 1] in {0}:
        Canevas.coords(image, a - 25, b, c - 25, d)
    
        '''
        Déplacement_2
        '''
        
def fctDroite2(ev=None):
    global compteur2
    global compteur_tour2
    [a, b, c, d] = Canevas.coords(image_2)              #Mouvement Droite 
    i = int((a - 50) // 25)
    j = int((b - 50) // 25)
    
    if L[j][i + 1] in {0, 6, 4}:
        Canevas.coords(image_2, a + 25, b, c + 25, d)
        if L[j][i + 1] == 6:
            command=compteur_tour2()
            print("Nombre de tours bleu :", compteur2)              #Compteur 
    

def fctHaut2(ev=None):
    [a, b, c, d] = Canevas.coords(image_2)                      #Mouvement Haut
    i = int((a - 50) // 25)
    j = int((b - 50) // 25)
    
    if L[j - 1][i] in {0, 6, 4}:
        Canevas.coords(image_2, a, b - 25, c, d - 25)
    

def fctBas2(ev=None):
    [a, b, c, d] = Canevas.coords(image_2)                    #Mouvement Bas
    i = int((a - 50) // 25)
    j = int((b - 50) // 25)
    
    if L[j + 1][i] in {0, 6, 4}:
        Canevas.coords(image_2, a, b + 25, c, d + 25)
    

def fctGauche2(ev=None):
    [a, b, c, d] = Canevas.coords(image_2)                    #Mouvement Gauche
    i = int((a - 50) // 25)
    j = int((b - 50) // 25)
    
    if L[j][i - 1] in {0}:
        Canevas.coords(image_2, a - 25, b, c - 25, d)       
       
        
##########################################################
##########    Variables ##################################
##########################################################
L=[[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,0,0,0,0,0,0,0,0,6,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,5,0,0,0,0,1,1],
   [1,0,0,0,0,0,0,0,0,0,6,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,5,0,0,0,0,1],
   [1,0,0,0,0,0,0,0,0,0,6,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1],
   [1,0,0,0,0,0,0,0,0,0,6,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,4,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1],
   [1,5,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1],
   [1,5,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,5,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1],
   [1,0,0,0,0,1,1,1,1,0,0,0,0,0,0,0,0,5,1,1,1,1,3,1,3,1,3,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1],
   [1,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,5,0,0,1,1,1,3,3,3,3,3,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,1],
   [1,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,3,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1],
   [1,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,1],
   [1,1,0,0,0,0,1,1,0,0,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,1],
   [1,1,0,0,0,0,1,1,0,0,5,0,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,1],
   [1,1,5,5,0,0,1,1,0,0,0,5,1,1,1,0,0,0,0,1,0,5,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,1,1],
   [1,1,0,0,0,0,1,1,0,0,0,0,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,0,0,0,0,1,1,0,0,0,0,1,3,1,0,0,0,0,1,0,0,0,0,0,1,0,5,0,0,0,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,0,0,0,0,1,1,0,0,0,0,1,1,1,0,0,0,0,1,0,0,0,0,1,1,1,5,5,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1],
   [1,1,0,0,5,5,1,1,0,0,0,0,1,3,1,5,0,0,0,1,0,0,0,0,1,1,1,5,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
   [1,1,0,0,0,0,1,1,5,0,0,0,1,1,1,5,0,0,0,1,0,0,0,0,1,3,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1,0,0,5,5,1,3,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,1,1,1,0,0,0,0,1,0,0,0,0,1,1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,1,3,1,0,0,0,0,1,0,0,0,0,1,3,1,0,0,0,0,1,1,1,1,3,3,3,1,1,1,1,1,1,0,0,0,0,1,1],
   [1,1,1,0,0,0,0,0,0,0,0,1,1,1,1,0,0,0,0,1,0,0,0,0,1,3,1,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,1,0,0,0,0,1,1,1,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
   [1,1,1,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1,5,5,0,0,1,1,1,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
   [1,1,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,5,5,1,0,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,1,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,5,5,0,0,1,1,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,5,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,1,1,3,3,1,1,1,1,3,3,1,1,1,1,1,1,1,1,1,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,1],
   [1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,5,5,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,1],
   [1,1,0,0,0,0,1,1,1,1,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,1],
   [1,1,0,0,0,0,1,1,1,1,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,1],
   [1,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1],
   [1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,0,0,0,0,0,0,1],
   [1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,1,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,5,5,0,0,0,0,1,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
   [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]]
#########################################################
########## Interface graphique ##########################
##########################################################



##################################################################[Voiture_1]
Mafenetre = Tk()
Mafenetre.title("Voiture")
Canevas = Canvas(Mafenetre,width=1500,height=1500,bg ='white')
Canevas.pack()
Canevas.create_rectangle(1300,50,1500,1500,fill='grey')
font = Font(family='Liberation Serif', size=200) # création d'une police pour l'affichage du texte
Canevas.create_text(1355,200,text="Rouge",fill="red",font=Font)
Canevas.create_text(1445,200,text="Bleu",fill="blue",font=Font)
Canevas.create_text(1400,100,text="SCORE",fill="black",font=Font)

bouton = Button(Mafenetre, text="Lancer", command=aff)
bouton.place(x=1310,y=403)

Lab = Label(Mafenetre, text=0, fg='black', bg='white')  # Initialisez le texte du Label à 0 #Timer
Lab.place(x=1365,y=405)

Com = Label(Mafenetre, text=0, fg='red', bg='white')  # Initialisez le texte du Label à 0#Compteur1
Com.place(x=1355,y=250)

Com2 = Label(Mafenetre, text=0, fg='blue', bg='white')  # Initialisez le texte du Label à 0#Compteur2
Com2.place(x=1445,y=250)
police = Font(family='Helvetica', size=24, weight='bold')
#######################

Time = 0  # Initialise le temps à 0
compteur=-1
compteur2=-1

###########################################################
########### Receptionnaire d'évènement ####################
###########################################################
Canevas.bind_all('<d>',fctDroite)#Droite
Canevas.bind_all('<z>',fctHaut)#Haut
Canevas.bind_all('<s>',fctBas)#Bas
Canevas.bind_all('<q>',fctGauche)#Gauche

Canevas.bind_all('<m>',fctDroite2)#Droite
Canevas.bind_all('<o>',fctHaut2)#Haut
Canevas.bind_all('<l>',fctBas2)#Bas
Canevas.bind_all('<k>',fctGauche2)#Gauche
##########################################################
############# Programme principal ########################
##########################################################
affiche(L)
image=Canevas.create_oval(275,75,250,100,fill='red')
image_2=Canevas.create_oval(275,150,250,175,fill='blue')
###################### FIN ###############################  
Mafenetre.mainloop()
